from flask import Flask, request, jsonify
from flask_restful import reqparse, abort, Api, Resource
# from KNN_DesitionTree import y_predict1_list
# from ranking import ranking
import simplejson as json
app = Flask(__name__)
api = Api(app)

def abort_if_todo_doesnt_exist(todo_id):
    if todo_id not in TODOS:
        abort(404, message="Todo {} doesn't exist".format(todo_id))

parser = reqparse.RequestParser()
parser.add_argument('task')

class hello(Resource):
    def get(self):
        print("is called")
        data = {
			'data': 'hello'
		}

        response = app.response_class(
    		response=json.dumps(data),
		    mimetype='application/json'
    	)
        return response

class Ranking_AI(Resource):
    # def get(self):
    #     print("is called")
    #     data = {
	# 		'data': str(y_predict1_list)
	# 	}

    #     response = app.response_class(
    # 		response=json.dumps(data),
	# 	    mimetype='application/json'
    # 	)
    #     return response

    def post(self):
        # args = parser.parse_args()
        print('this is args')
        date = request.form['date']
        t = request.form['t']

        data = {
            str(ranking(str(date),str(t)) # EX: date => 1-1-2018, t => hotel
            )}

        response = app.response_class(
    		response=json.dumps(data),
		    mimetype='application/json'
        )
    
        return response

class prediction(Resource):
    def get(self):
        print("is called")
        data = {
			'data': str(y_predict1_list)
		}

        response = app.response_class(
    		response=json.dumps(data),
		    mimetype='application/json'
    	)
        return response

##
## Actually setup the Api resource routing here
##
api.add_resource(hello, '/')
api.add_resource(Ranking_AI, '/ranking')
api.add_resource(prediction, '/prediction')


if __name__ == '__main__':
    app.run(debug=True, port='5241')
